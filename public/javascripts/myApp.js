var app = angular.module('app', ['ngResource', 'ngRoute']);

app.config(['$routeProvider', function($routeProvider){
    $routeProvider
        .when('/items', {
    		templateUrl: 'partials/listing.html',
    		controller: 'ListingCtrl'
		})
		.when('/add-item', {
            templateUrl: 'partials/add-item.html',
            controller: 'AddItemCtrl'
        })
        .when('/item/:id', {
            templateUrl: 'partials/item-details.html',
            controller: 'DetailsCtrl'
        })
        .when('/item/edit/:id', {
            templateUrl: 'partials/edit-item.html',
            controller: 'EditItemCtrl'
        })
        .when('/item/delete/:id', {
            templateUrl: 'partials/delete-item.html',
            controller: 'DeleteItemCtrl'
        })
        .when('/item/bidhistory/:id', {
            templateUrl: 'partials/bidhistory.html',
            controller: 'AllBidsCtrl'
        })
        .otherwise({
            redirectTo: '/items'
        });
}]);

app.controller('ListingCtrl', ['$scope', '$resource', '$rootScope', '$location',
    function($scope, $resource, $rootScope, $location){
        var Items = $resource('/api/items');
        var Users = $resource('/users');
        var Itemslength = 0;
        var Userslength = 0;
        
        Items.query(function(items){
            $scope.items = items;
            Itemslength = items.length;
        });
        Users.query(function(users) {
            $scope.users = users;
            Userslength = users.length;
        });


        for (var i=0; i < length; ++i) {

            //need to set the dates manually
            $scope.items[i].endDate = new Date(items[i].endDate);
            $scope.items[i].uploadDate = new Date(items[i].uploadDate);
        }

        //check if the user just loaded the page and has not logged in
        if(typeof $rootScope.username === "undefined") {
            $rootScope.username = "visitor";
            $rootScope.loggedIn = false;
        }


        $scope.login = function() {
            Users.query(function(users) {
                $scope.users = users;
                Userslength = users.length;
            });

            var userFound = false;

            for(var i=0; i < Userslength; ++i){
                if($scope.user == $scope.users[i].username) { 
                    userFound = true;

                    //hash the password and compare it to the hashed password stored
                    //in our mongodb database

                    var newhash = CryptoJS.MD5($scope.password);

                    if (newhash == $scope.users[i].password) {

                        $rootScope.username = $scope.user;
                        $rootScope.loggedIn = true;

                        //reset the username and password fields
                        $scope.password = "";
                        $scope.user = "";

                        updatePage();

                    }
                    else {
                        alert("Incorrect password");
                    }
                }
            }

            if(!userFound)
                alert("That user does not exist");
        };

        $scope.register = function() {
            Users.query(function(users) {
                $scope.users = users;
                Userslength = users.length;
            });

            var exists = false;
            
            for(var i=0; i < Userslength; ++i){
                 if($scope.user == $scope.users[i].username) {
                    exists = true;
                 }
            }

            if(exists)
                alert("User Already Exists");
            else {
                //hash the password
                var hash = String( CryptoJS.MD5($scope.password) );
                
                var newUser = new Object();
                newUser.password = hash;
                newUser.username = $scope.user;
                
                //call the express method to update Mongodb               
                Users.save(newUser, function(){
                    $rootScope.username = $scope.user;
                    $rootScope.loggedIn = true;

                    //reset the username and password fields
                    $scope.password = "";
                    $scope.user = "";
                    
                    updatePage();
                });
            }
        };

        $scope.logout = function() {
            $rootScope.loggedIn = false;
            $rootScope.username = "visitor";

            //reset the username and password fields
            $scope.password = "";
            $scope.user = "";
            
            updatePage();
        }

        function updatePage() {
            //set a scope variable so that we can display the username on the page
            $scope.currUser = $rootScope.username;
            $location.path('/items');
        }

        $scope.currUser = $rootScope.username;

    }]);

app.controller('DetailsCtrl', ['$scope', '$resource', '$location', '$routeParams', '$rootScope',
    function($scope, $resource, $location, $routeParams, $rootScope){   
        var Items = $resource('/api/items/:id', { id: '@_id' }, {
            update: { method: 'PUT' }
        });

        Items.get({ id: $routeParams.id }, function(item){
            $scope.item = item;

            //need to set the dates manually
            $scope.item.endDate = new Date(item.endDate);
            $scope.item.uploadDate = new Date(item.uploadDate);

            //set the bid view correctly
            $scope.newbid = $scope.item.bid;
        });



        $scope.bid = function(){

            //if the user accessing the page is not logged in or is not the 
            if(!$rootScope.loggedIn) {
                alert("You must be logged in to bid.")
            }
            else {
                //can only bid up, not down
                if($scope.newbid > $scope.item.bid) {

                    //if the auction is expired
                    if($scope.item.endDate.getTime() > new Date().getTime()) {
                        
                        $scope.item.bid = $scope.newbid;
                        $scope.item.allbids.push($rootScope.username + " bid $" + $scope.newbid);    

                        Items.update($scope.item, function(){
                            $location.path('/items');
                        });
                    }
                    else
                        alert("EXPIRED");
                }
            }
        };
    }]);

app.controller('AddItemCtrl', ['$scope', '$resource', '$location', '$rootScope',
    function($scope, $resource, $location, $rootScope){


        if(!$rootScope.loggedIn) {
            $location.path('/items');
        }

        $scope.save = function(){
            var Items = $resource('/api/items');

            $scope.item.seller = $rootScope.username; //set the seller to the logged in user
            $scope.item.uploadDate = new Date(); //set the upload date to the current date

            Items.save($scope.item, function(){
                $location.path('/items');
            });
        };
    }]);

app.controller('EditItemCtrl', ['$scope', '$resource', '$location', '$routeParams', '$rootScope',
    function($scope, $resource, $location, $routeParams, $rootScope){   
        var Items = $resource('/api/items/:id', { id: '@_id' }, {
            update: { method: 'PUT' }
        });

        Items.get({ id: $routeParams.id }, function(item){
            $scope.item = item;

            //redirect the user to back to the details page if they are not logged in 
            //or not the seller
            if(!($rootScope.loggedIn && $rootScope.username == $scope.item.seller)) {
                $location.path("/item/"+$routeParams.id);
            }

            //need to set the dates manually
            $scope.item.endDate = new Date(item.endDate);
            $scope.item.uploadDate = new Date(item.uploadDate);
        });

        $scope.save = function(){
            Items.update($scope.item, function(){
                $location.path('/items');
            });
        };
    }]);

app.controller('DeleteItemCtrl', ['$scope', '$resource', '$location', '$routeParams', '$rootScope',
    function($scope, $resource, $location, $routeParams, $rootScope){
        var Items = $resource('/api/items/:id');

        Items.get({ id: $routeParams.id }, function(item){
            $scope.item = item;

            //redirect the user to back to the details page if they are not logged in 
            //or not the seller
            if(!($rootScope.loggedIn && $rootScope.username == $scope.item.seller)) {
                $location.path("/item/"+$routeParams.id);
            }
        });

        $scope.delete = function(){

            Items.delete({ id: $routeParams.id }, function(item){
                $location.path('/items');
            });
        };
    }]);

app.controller('AllBidsCtrl', ['$scope', '$resource', '$location', '$routeParams', '$rootScope',
    function($scope, $resource, $location, $routeParams, $rootScope){
        var Items = $resource('/api/items/:id');

        Items.get({ id: $routeParams.id }, function(item){
            $scope.item = item;

            //redirect the user to back to the details page if they are not logged in 
            //or not the seller
            if(!($rootScope.loggedIn && $rootScope.username == $scope.item.seller)) {
                $location.path("/item/"+$routeParams.id);
            }

        });

    }]);


