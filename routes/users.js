var express = require('express');
var router = express.Router();

var monk = require('monk');
var db = monk('localhost:27017/bidding');

//router for logging in 
router.get('/', function(req, res) {
    var collection = db.get('users');
    collection.find({}, function(err, users){
        if (err) throw err;
        res.json(users);
    });
});

router.post('/', function(req, res){
    var collection = db.get('users');
    collection.insert({
        username: req.body.username,
        password: req.body.password,
        
    }, function(err, user){
        if (err) throw err;

        res.json(user);
    });
});


module.exports = router;
