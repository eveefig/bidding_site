var express = require('express');
var router = express.Router();

var monk = require('monk');
var db = monk('localhost:27017/bidding');

//routers for controlling items

router.get('/', function(req, res) {
    var collection = db.get('items');
    collection.find({}, function(err, items){
        if (err) throw err;
      	res.json(items);
    });
});

router.post('/', function(req, res){
    var collection = db.get('items');
    var allbids = new Array();
    allbids.push("Starting Bid: $" + req.body.bid);

    collection.insert({
        name: req.body.name,
        description: req.body.description,
        seller: req.body.seller,
        bid: req.body.bid,
        endDate: new Date(req.body.endDate),
        uploadDate: new Date(req.body.uploadDate),
        allbids: allbids,
        
    }, function(err, item){
        if (err) throw err;

        res.json(item);
    });
});

router.get('/:id', function(req, res) {
    var collection = db.get('items');
    collection.findOne({ _id: req.params.id }, function(err, item){
        if (err) throw err;

      	res.json(item);
    });
});

router.put('/:id', function(req, res){
    var collection = db.get('items');
    collection.update({
        _id: req.params.id
    },
    {
        name: req.body.name,
        description: req.body.description,
        endDate: new Date(req.body.endDate),
        uploadDate: new Date(req.body.uploadDate),
        seller: req.body.seller,
        bid: req.body.bid,
        allbids: req.body.allbids,

    }, function(err, item){
        if (err) throw err;

        res.json(item);
    });
});

router.delete('/:id', function(req, res){
    var collection = db.get('items');
    collection.remove({ _id: req.params.id }, function(err, item){
        if (err) throw err;

        res.json(item);
    });
});


module.exports = router;